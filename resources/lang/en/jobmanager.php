<?php
/**
 * Created by PhpStorm.
 * User: idist
 * Date: 30/07/2018
 * Time: 08:42
 */
return [
    'default' => [
        'header' => 'Job Information',
        'description' => 'Description',
        'salary' => 'Salary',
        'purposes' => 'Purposes',
        'expire' => 'Expire',
        'active' => 'Active',
        'heading' => 'Heading',
        'sub_heading' => 'Sub Heading',
    ],
    'apply_button' => 'Xem công việc này',
    'frontend' => [
        'title' => [
            'description' => 'Thông tin',
            'purposes' => 'Yêu cầu',

        ],
        'apply_button' => 'Nộp CV',
        'salary' => 'Mức hỗ trợ',
        'description' => 'Mô tả công việc',
        'purposes' => 'Yêu cầu của công việc'
    ]

];

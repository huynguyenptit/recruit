<?php
/**
 * Created by PhpStorm.
 * User: idist
 * Date: 07/08/2018
 * Time: 21:33
 */

return [
    'admin' => [
        'apply' => [
            'review' => 'Review CV',
            'header' => 'CV của <b>:name</b> apply vào job: :job',
        ]
    ],
    'frontend' => [
        'title' => [
            'basic_information' => 'Thông tin cơ bản'
        ]
    ],
    'create_new' => 'Tạo CV mới',
    'edit_button' => 'Chỉnh sửa CV này',
    'apply_button' => 'Nộp CV này',
];
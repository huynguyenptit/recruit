<?php

return [
    'message' => 'Please return to <a href=\':url\'>our homepage</a>.',
    '400' =>[
        'title' =>'Error 400',
        'code'=>'400',
        'error_title' => 'Bad request.',
        'message' => 'Please return to <a href=\':url\'>our homepage</a>.',
    ],
    '401' =>[
        'code'=>'401',
        'error_title' => 'Unauthorized action.',
        'message' => 'Please return to <a href=\':url\'>our homepage</a>.',
    ],
    '403' =>[
        'code'=>'403',
        'error_title' => 'Forbidden.',
        'message' => 'Please return to <a href=\':url\'>our homepage</a>.',
    ],
    '404' =>[
        'code'=>'404',
        'error_title' => 'Page not found.',
        'message' => 'Please return to <a href=\':url\'>our homepage</a>.',
    ],
    '405' =>[
        'code'=>'405',
        'error_title' => 'Method not allowed.',
        'message' => 'Please return to <a href=\':url\'>our homepage</a>.',
    ],
    '408' =>[
        'code'=>'408',
        'error_title' => 'Request timeout.',
        'message' => 'Please return to <a href=\':url\'>our homepage</a>.',
    ],
    '429' =>[
        'code'=>'429',
        'error_title' => 'Too many requests.',
        'message' => 'Please return to <a href=\':url\'>our homepage</a>.',
    ],
    '500' =>[
        'code'=>'500',
        'error_title' => 'It\'s not you, it\'s me.',
        'message' => 'An internal server error has occurred. If the error persists please contact the development team.',
    ],
    '503' =>[
        'code'=>'503',
        'error_title' => 'It\'s not you, it\'s me.',
        'message' => 'The server is overloaded or down for maintenance. Please try again later.',
    ],
    'save_fail' => 'Qúa trình lưu xảy ra lỗi, vui lòng liên hệ admin để được hỗ trợ!',
    'save_success' => 'Lưu thành công!',

];

<?php
/**
 * Created by PhpStorm.
 * User: idist
 * Date: 29/07/2018
 * Time: 09:45
 */
return [
  'default' => [
      'banner' => [
          'active' => 'Active',
          'header' => 'Banner',
          'background' => 'Banner Background',
      ],
      'left_banner' => [
          'active' => 'Active',
          'header' => 'Left Banner',
          'title' => 'Left Banner Title',
          'description' => 'Description',
          'button_title' => 'Button Title',
          'button_icon' => 'Icon',
          'button_link' => 'Link',
          'background_title' => 'Left Banner Background',
      ],
      'right_banner' => [
          'active' => 'Active',
          'header' => 'Right Banner',
          'title' => 'Right Banner Title',
          'description' => 'Description',
          'button_title' => 'Button Title',
          'button_icon' => 'Icon',
          'button_link' => 'Link',
          'background_title' => 'Right Banner Background'
      ],
      'inforgraphy' => [
          'active' => 'Active',
          'header' => 'Inforgraphy',
          'title' => 'Inforgraphy Title',
          'content' => 'Inforgraphy Content',
          'table' => [
            'thumbnail' => 'Thumbnails',
            'title' => 'Titles',
            'link' => 'Links',
          ],
          'max' => 3,
      ],
      'reason' => [
          'active' => 'Active',
          'header' => 'Reason',
          'title' => 'Reason Title',
          'content' => 'Reason Content',
          'table' => [
              'thumbnail' => 'Thumbnails',
              'title' => 'Titles',
              'link' => 'Links',
              'description' => 'Descriptions'
          ],
      ],
      'member' => [
          'active' => 'Active',
          'header' => 'Members',
          'title' => 'Member Title',
          'content' => 'Member Content',
          'table' => [
              'avatar' => 'Avatar Link',
              'name' => 'Name',
              'position' => 'Position',
              'comment' => 'Comment'
          ],
      ]

  ]
];

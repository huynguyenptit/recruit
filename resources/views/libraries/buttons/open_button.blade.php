<a class="btn btn-default btn-xs" href="{{ $link!=null?$link:'#' }}" target="_blank">
    <i class="fa fa-eye"></i>
    {{ trans('backpack::pagemanager.open')  }}
</a>

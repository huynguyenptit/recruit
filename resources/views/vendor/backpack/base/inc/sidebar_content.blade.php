<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li>
    <a href="{{ backpack_url('dashboard') }}">
        <i class="fa fa-dashboard"></i>
        <span>Dashboard</span>
    </a>
</li>
{{--<li>--}}
    {{--<a href="{{ url(config('backpack.base.route_prefix') . '/page') }}">--}}
        {{--<i class="fa fa-file-o"></i>--}}
        {{--<span>Pages</span>--}}
    {{--</a>--}}
{{--</li>--}}
<li>
    <a href="{{ url(config('backpack.base.route_prefix') . '/job') }}">
        <i class="fa fa-file-o"></i>
        <span>Jobs</span>
    </a>
</li>
{{--<li>--}}
    {{--<a href="{{ url(config('backpack.base.route_prefix') . '/apply') }}">--}}
        {{--<i class="fa fa-file-o"></i>--}}
        {{--<span>{{ trans('cv.admin.sidebar_menu.title') }}</span>--}}
    {{--</a>--}}
{{--</li>--}}
<li>
    <a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/menu-item') }}">
        <i class="fa fa-list"></i>
        <span>Menu</span>
    </a>
</li>
<!-- Users, Roles Permissions -->
<li class="treeview">
    <a href="#"><i class="fa fa-group"></i> <span>Quản lý người dùng</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('user') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
        <li><a href="{{ backpack_url('role') }}"><i class="fa fa-group"></i> <span>Roles</span></a></li>
        <li><a href="{{ backpack_url('permission') }}"><i class="fa fa-key"></i> <span>Permissions</span></a></li>
    </ul>
</li>
<li>
    <a href="{{ backpack_url('elfinder') }}">
        <i class="fa fa-files-o"></i>
        <span>Quản lý File</span>
    </a>
</li>
<li>
    <a href='{{ url(config('backpack.base.route_prefix', 'admin') . '/backup') }}'>
        <i class='fa fa-hdd-o'></i>
        <span>Backups</span>
    </a>
</li>
<li>
    <a href='{{ url(config('backpack.base.route_prefix', 'admin') . '/log') }}'>
        <i class='fa fa-terminal'></i>
        <span>Logs</span>
    </a>
</li>
<li>
    <a href='{{ url(config('backpack.base.route_prefix', 'admin') . '/setting') }}'>
        <i class='fa fa-cog'></i>
        <span>Settings</span>
    </a>
</li>
